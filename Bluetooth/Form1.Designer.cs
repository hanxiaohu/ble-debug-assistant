﻿namespace Bluetooth
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_ble_scan = new System.Windows.Forms.Button();
            this.textBox_Recv = new System.Windows.Forms.TextBox();
            this.btn_send = new System.Windows.Forms.Button();
            this.btn_clean_send = new System.Windows.Forms.Button();
            this.timer_ble_scan = new System.Windows.Forms.Timer(this.components);
            this.listviewBleDevice = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.comboBoxWirteChart = new System.Windows.Forms.ComboBox();
            this.labelWriteUUID = new System.Windows.Forms.Label();
            this.textBox_Send = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listViewChar = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.writeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listViewServer = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listViewCmd = new System.Windows.Forms.ListView();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listboxMessage = new System.Windows.Forms.ListBox();
            this.btn_clean_recv = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_ble_scan
            // 
            this.btn_ble_scan.Location = new System.Drawing.Point(589, 181);
            this.btn_ble_scan.Name = "btn_ble_scan";
            this.btn_ble_scan.Size = new System.Drawing.Size(90, 32);
            this.btn_ble_scan.TabIndex = 2;
            this.btn_ble_scan.Text = "扫描蓝牙";
            this.btn_ble_scan.UseVisualStyleBackColor = true;
            this.btn_ble_scan.Click += new System.EventHandler(this.btn_ble_scan_Click);
            // 
            // textBox_Recv
            // 
            this.textBox_Recv.Location = new System.Drawing.Point(0, 0);
            this.textBox_Recv.Multiline = true;
            this.textBox_Recv.Name = "textBox_Recv";
            this.textBox_Recv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_Recv.Size = new System.Drawing.Size(571, 354);
            this.textBox_Recv.TabIndex = 5;
            // 
            // btn_send
            // 
            this.btn_send.Location = new System.Drawing.Point(714, 19);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(80, 32);
            this.btn_send.TabIndex = 7;
            this.btn_send.Text = "发送";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // btn_clean_send
            // 
            this.btn_clean_send.Location = new System.Drawing.Point(715, 76);
            this.btn_clean_send.Name = "btn_clean_send";
            this.btn_clean_send.Size = new System.Drawing.Size(80, 32);
            this.btn_clean_send.TabIndex = 8;
            this.btn_clean_send.Text = "清除发送";
            this.btn_clean_send.UseVisualStyleBackColor = true;
            this.btn_clean_send.Click += new System.EventHandler(this.btn_clean_send_Click);
            // 
            // timer_ble_scan
            // 
            this.timer_ble_scan.Interval = 1000;
            this.timer_ble_scan.Tick += new System.EventHandler(this.timer_ble_scan_Tick);
            // 
            // listviewBleDevice
            // 
            this.listviewBleDevice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listviewBleDevice.FullRowSelect = true;
            this.listviewBleDevice.GridLines = true;
            this.listviewBleDevice.HideSelection = false;
            this.listviewBleDevice.Location = new System.Drawing.Point(580, 32);
            this.listviewBleDevice.Name = "listviewBleDevice";
            this.listviewBleDevice.Size = new System.Drawing.Size(219, 143);
            this.listviewBleDevice.TabIndex = 9;
            this.listviewBleDevice.UseCompatibleStateImageBehavior = false;
            this.listviewBleDevice.View = System.Windows.Forms.View.Details;
            this.listviewBleDevice.SelectedIndexChanged += new System.EventHandler(this.listviewBleDevice_SelectedIndexChanged);
            this.listviewBleDevice.DoubleClick += new System.EventHandler(this.listviewBleDevice_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "MAC";
            this.columnHeader1.Width = 84;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 110;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(578, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "蓝牙选择";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 360);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(809, 180);
            this.tabControl1.TabIndex = 11;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.comboBoxWirteChart);
            this.tabPage1.Controls.Add(this.labelWriteUUID);
            this.tabPage1.Controls.Add(this.textBox_Send);
            this.tabPage1.Controls.Add(this.btn_send);
            this.tabPage1.Controls.Add(this.btn_clean_send);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(801, 154);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "发送";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // comboBoxWirteChart
            // 
            this.comboBoxWirteChart.FormattingEnabled = true;
            this.comboBoxWirteChart.Location = new System.Drawing.Point(3, 112);
            this.comboBoxWirteChart.Name = "comboBoxWirteChart";
            this.comboBoxWirteChart.Size = new System.Drawing.Size(564, 20);
            this.comboBoxWirteChart.TabIndex = 10;
            // 
            // labelWriteUUID
            // 
            this.labelWriteUUID.AutoSize = true;
            this.labelWriteUUID.Location = new System.Drawing.Point(6, 135);
            this.labelWriteUUID.Name = "labelWriteUUID";
            this.labelWriteUUID.Size = new System.Drawing.Size(65, 12);
            this.labelWriteUUID.TabIndex = 9;
            this.labelWriteUUID.Text = "未选择服务";
            // 
            // textBox_Send
            // 
            this.textBox_Send.Location = new System.Drawing.Point(4, 4);
            this.textBox_Send.Multiline = true;
            this.textBox_Send.Name = "textBox_Send";
            this.textBox_Send.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_Send.Size = new System.Drawing.Size(693, 104);
            this.textBox_Send.TabIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listViewChar);
            this.tabPage2.Controls.Add(this.listViewServer);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(801, 154);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "服务";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listViewChar
            // 
            this.listViewChar.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.listViewChar.ContextMenuStrip = this.contextMenuStrip1;
            this.listViewChar.FullRowSelect = true;
            this.listViewChar.GridLines = true;
            this.listViewChar.HideSelection = false;
            this.listViewChar.Location = new System.Drawing.Point(302, 3);
            this.listViewChar.Name = "listViewChar";
            this.listViewChar.Size = new System.Drawing.Size(496, 148);
            this.listViewChar.TabIndex = 3;
            this.listViewChar.UseCompatibleStateImageBehavior = false;
            this.listViewChar.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Characteristic UUID";
            this.columnHeader3.Width = 256;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Write";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Read";
            this.columnHeader5.Width = 50;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Notify";
            this.columnHeader6.Width = 50;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeToolStripMenuItem,
            this.notifyToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(112, 48);
            // 
            // writeToolStripMenuItem
            // 
            this.writeToolStripMenuItem.Name = "writeToolStripMenuItem";
            this.writeToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.writeToolStripMenuItem.Text = "Write";
            this.writeToolStripMenuItem.Click += new System.EventHandler(this.writeToolStripMenuItem_Click);
            // 
            // notifyToolStripMenuItem
            // 
            this.notifyToolStripMenuItem.Name = "notifyToolStripMenuItem";
            this.notifyToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.notifyToolStripMenuItem.Text = "Notify";
            this.notifyToolStripMenuItem.Click += new System.EventHandler(this.notifyToolStripMenuItem_Click);
            // 
            // listViewServer
            // 
            this.listViewServer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7});
            this.listViewServer.FullRowSelect = true;
            this.listViewServer.GridLines = true;
            this.listViewServer.HideSelection = false;
            this.listViewServer.Location = new System.Drawing.Point(3, 3);
            this.listViewServer.Name = "listViewServer";
            this.listViewServer.Size = new System.Drawing.Size(296, 148);
            this.listViewServer.TabIndex = 2;
            this.listViewServer.UseCompatibleStateImageBehavior = false;
            this.listViewServer.View = System.Windows.Forms.View.Details;
            this.listViewServer.SelectedIndexChanged += new System.EventHandler(this.listViewServer_SelectedIndexChanged);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Service UUID";
            this.columnHeader7.Width = 256;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.listViewCmd);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(801, 154);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "操作";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // listViewCmd
            // 
            this.listViewCmd.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.listViewCmd.FullRowSelect = true;
            this.listViewCmd.GridLines = true;
            this.listViewCmd.HideSelection = false;
            this.listViewCmd.Location = new System.Drawing.Point(3, 3);
            this.listViewCmd.Name = "listViewCmd";
            this.listViewCmd.Size = new System.Drawing.Size(795, 149);
            this.listViewCmd.TabIndex = 0;
            this.listViewCmd.UseCompatibleStateImageBehavior = false;
            this.listViewCmd.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "操作";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "服务UUID";
            this.columnHeader9.Width = 256;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "特征UUID";
            this.columnHeader10.Width = 256;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listboxMessage);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(801, 154);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "日志";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listboxMessage
            // 
            this.listboxMessage.FormattingEnabled = true;
            this.listboxMessage.ItemHeight = 12;
            this.listboxMessage.Location = new System.Drawing.Point(3, 3);
            this.listboxMessage.Name = "listboxMessage";
            this.listboxMessage.Size = new System.Drawing.Size(795, 148);
            this.listboxMessage.TabIndex = 3;
            // 
            // btn_clean_recv
            // 
            this.btn_clean_recv.Location = new System.Drawing.Point(589, 322);
            this.btn_clean_recv.Name = "btn_clean_recv";
            this.btn_clean_recv.Size = new System.Drawing.Size(90, 32);
            this.btn_clean_recv.TabIndex = 12;
            this.btn_clean_recv.Text = "清除接收";
            this.btn_clean_recv.UseVisualStyleBackColor = true;
            this.btn_clean_recv.Click += new System.EventHandler(this.btn_clean_recv_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(710, 182);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 543);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_clean_recv);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listviewBleDevice);
            this.Controls.Add(this.textBox_Recv);
            this.Controls.Add(this.btn_ble_scan);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_ble_scan;
        private System.Windows.Forms.TextBox textBox_Recv;
        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.Button btn_clean_send;
        private System.Windows.Forms.Timer timer_ble_scan;
        private System.Windows.Forms.ListView listviewBleDevice;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox_Send;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListBox listboxMessage;
        private System.Windows.Forms.ListView listViewChar;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ListView listViewServer;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Button btn_clean_recv;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem writeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notifyToolStripMenuItem;
        private System.Windows.Forms.Label labelWriteUUID;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ListView listViewCmd;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ComboBox comboBoxWirteChart;
        private System.Windows.Forms.Button button1;
    }
}

